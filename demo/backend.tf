
# Copying terraform.tfstate to remote backend s3
terraform {
  backend "s3" {
    bucket              = "terraform-demo-remote-s3"
    key                 = "terraform.tfstate"
    region              = "us-east-2"
  }
}