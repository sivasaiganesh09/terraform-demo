output "elb_ip" {
    description = "Elastic Load balancer ip"
    value = module.ec2.elb_publicip
}

output "mysql_url" {
    description = "MySQL URL"
    value = module.ec2.rds_hostname
}

output "mysql_username" {
    description = "MySQL Username"
    value = module.ec2.rds_username
    sensitive = true
}

output "mysql_password" {
    description = "MySQL Password"
    value = module.ec2.rds_password
    sensitive = true
}