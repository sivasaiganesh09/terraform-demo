# terraform-demo

# Clone the repository 
git clone https://gitlab.com/sivasaiganesh09/terraform-demo.git

# Change the directory to the cloned repository
cd terraform-demo/demo

# Run the terraform commands
terraform init

terraform plan

terraform apply --auto-approve
